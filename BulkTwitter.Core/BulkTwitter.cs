﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;

namespace IKesler.BulkTwitter.Core
{
    public class BulkTwitter
    {
        private readonly TwitterService service;
        private OAuthRequestToken requestToken;
        private OAuthAccessToken access;

        public bool IsLoggedIn
        {
            get { return access != null; }
        }

        public BulkTwitter()
        {
            service = new TwitterService("vFfnBWbTHKl7VbjqoNHnhQ", "g86yiX1oLxKgUDROBEtoOtJYEvpYMXQKy7J1gERQsN8");
        }

        public IEnumerable<Tweet> GetTopTweets(int count)
        {
            var tweets =
                service.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions { Count = count,  });
            return tweets.Select(t => new Tweet
            {
                Id = t.Id,
                Text = t.Text,
                PostedDate = t.CreatedDate
            });
        }

        public string GetLoginUrl()
        {
            requestToken = service.GetRequestToken();
            return service.GetAuthorizationUri(requestToken).ToString();
        }

        public AccessToken Login(string pin)
        {
            access = service.GetAccessToken(requestToken, pin);
            service.AuthenticateWith(access.Token, access.TokenSecret);
            return new AccessToken
            {
                ScreenName = access.ScreenName,
                Token = access.Token,
                TokenSecret = access.TokenSecret,
                UserId = access.UserId
            };
        }

        public void Login(AccessToken token)
        {
            access = new OAuthAccessToken
            {
                ScreenName = token.ScreenName,
                Token = token.Token,
                TokenSecret = token.TokenSecret,
                UserId = token.UserId
            };
            service.AuthenticateWith(access.Token, access.TokenSecret);
        }

        public void DeleteTweet(Tweet tweet)
        {
            service.DeleteTweet(new DeleteTweetOptions { Id = tweet.Id });
        }
    }
}
