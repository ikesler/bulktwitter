﻿namespace IKesler.BulkTwitter.Core
{
    public class AccessToken
    {
        public string Token { get; set; }

        public string TokenSecret { get; set; }

        public int UserId { get; set; }

        public string ScreenName { get; set; }
    }
}
