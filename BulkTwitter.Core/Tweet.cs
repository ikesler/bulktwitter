﻿using System;

namespace IKesler.BulkTwitter.Core
{
    public class Tweet
    {
        public long Id { get; internal set; }
        public string Text { get; internal set; }
        public DateTime PostedDate { get; internal set; }
    }
}
