﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IKesler.BulkTwitter.Core;

namespace DesktopUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private BulkTwitter core = new BulkTwitter();

        private void button1_Click(object sender, EventArgs e)
        {
            var url = core.GetLoginUrl();
            Process.Start(url);
        }

        private bool Login()
        {
            var result = false;
            //1. Try using saved token
            if (File.Exists("token"))
            {
                var pair = File.ReadAllLines("token");
                var token = new AccessToken { Token = pair[0], TokenSecret = pair[1] };
                core.Login(token);
                result = true;
            }
            //2. Use PIN
            else if (!string.IsNullOrEmpty(textBoxPin.Text))
            {
                var token = core.Login(textBoxPin.Text);
                File.WriteAllText("token", token.Token + "\n" + token.TokenSecret + "\n");
                result = true;
            }
            else
            {
                MessageBox.Show("No login info found. Try to get PIN.");
            }

            return result;
        }

        private void buttonLoadList_Click(object sender, EventArgs e)
        {
            if (!core.IsLoggedIn && !Login())
                return;
            int count;
            int.TryParse(textBoxCount.Text, out count);
            var tweets = core.GetTopTweets(count);
            listViewTweets.Items.Clear();
            foreach (var tweet in tweets)
            {
                var t = new ListViewItem();
                t.Text = tweet.Id.ToString();
                t.SubItems.Add(tweet.Text);
                t.SubItems.Add(tweet.PostedDate.ToString());
                t.Tag = tweet;
                listViewTweets.Items.Add(t);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            labelDeleteProgress.Text = "";
            var total = listViewTweets.CheckedItems.Count;
            var processed = 0;
            foreach (var item in listViewTweets.CheckedItems)
            {
                var tweet = (Tweet) ((ListViewItem) item).Tag;
                core.DeleteTweet(tweet);
                ++processed;
                labelDeleteProgress.Text = string.Format("{0} of {1} have been removed.", processed, total);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var item in listViewTweets.Items)
                ((ListViewItem) item).Checked = true;
        }
    }
}
